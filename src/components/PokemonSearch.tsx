import React, { Component } from 'react'
import User from '../interfaces/User.interface'
import SearchState from '../interfaces/SearchState.interface'

export class PokemonSearch extends Component<User, SearchState> {
	pokemonRef: React.RefObject<HTMLInputElement>;
	constructor(props: User) {
		super(props);
		this.state = {
			error: false,
			Pokemon: null, 
		}
		this.pokemonRef = React.createRef();
	}

	onSearchClick = (): void => {
		const inputValue = this.pokemonRef.current.value;
		fetch(`https://pokeapi.co/api/v2/pokemon/${inputValue}`)
			.then(res=> {
				if (res.status !== 200) {
					this.setState({error: true});
					return;
				}
				res.json().then(data => {
					this.setState({
						error: false,
						Pokemon: {
							name: data.name,
							numberOfAbilities: data.abilities.length,
							baseExperience: data.base_experience,
							imageUrl: data.sprites.front_default
						}
					})
				})
			})
	}

	firstUppercase = (word: string): string => {
		return word.charAt(0).toUpperCase() + word.slice(1);
	}

	render() {
		const { name: UserName, numberOfPokemon } = this.props;
		const { 
			error, 
			Pokemon 
		} = this.state;

		let resultMarkup;

		if (error) {
			resultMarkup = <p>Pokemon not found, please try again</p>
		} else  if (this.state.Pokemon){
			resultMarkup = <div className="my-result">
				<img src={Pokemon.imageUrl} alt="pokemon" className="pokemon-image" />
				<p>
					{this.firstUppercase(Pokemon.name)} has {Pokemon.numberOfAbilities} abilities and {' '}{Pokemon.baseExperience} base experience points.
				</p>
			</div>
		}

		return (
			<div>
				<p> User {UserName}{' '}
				{numberOfPokemon && <span>has {numberOfPokemon} Pokemon</span>}</p>

				<input type="text" ref={this.pokemonRef} className="my-input"/>
				<button onClick={this.onSearchClick} className="my-button">
					Search
				</button>
				{resultMarkup}
			</div>
		)
	}
}

export default PokemonSearch