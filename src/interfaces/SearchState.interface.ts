import Pokemon from './Pokemon.interface'

export default interface SearchState {
    error: boolean;
    Pokemon: Pokemon
}